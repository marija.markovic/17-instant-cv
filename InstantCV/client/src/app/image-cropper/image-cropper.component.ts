import { Injectable } from '@angular/core';
import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import Cropper from "cropperjs";
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.css']
})
@Injectable()
export class ImageCropperComponent implements OnInit {

  @ViewChild("image", { static: false })
  private imageElement: ElementRef;


  @Input("src")
  public imageSrc: string = "./assets/img/profile.png";

  public imageDest: string;
  private cropper: Cropper;
  private addedPicture: boolean = false;

  constructor(private _router: Router,
              public sanitizer: DomSanitizer,
              private http: HttpClient,
              private auth_service: AuthService ) { 
    this.imageDest = "";
  }

  public ngOnInit(): void {
    if(!this.auth_service.getID()){
      this._router.navigateByUrl("/choose-template");
    }
  }


  public ngAfterViewInit(){
    
    this.cropper = new Cropper(this.imageElement.nativeElement, {
      zoomable: true,
      scalable: false,
      aspectRatio: 1,
      crop: () => {
        const canvas = this.cropper.getCroppedCanvas();
        this.imageDest = canvas.toDataURL("image/png");
      }

    });
  }

  selectImage(event) {
    if (event.target.files) {
      const file = event.target.files[0];
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (event: any) => {
        
        this.imageSrc = event.target.result;
        this.cropper.replace(this.imageSrc);
        this.addedPicture = true;
      }
    }
  }

  public getImage(): string {
    //console.log("novo");
    return this.imageSrc;
  } 

  public uploadPicture(){
    if(this.addedPicture){
      this.http.post<any>(`${environment.serverUrl}/user/update/change_pic/${this.auth_service.getID()}`,
                            {'image': this.imageDest},
                            {'headers': this.auth_service.setTokenHeader()})
                            .subscribe(data => {
                              console.log(data);
                            });
                  this._router.navigateByUrl('/profile');
                  console.log("uploaded picture");
    }
  }
}
