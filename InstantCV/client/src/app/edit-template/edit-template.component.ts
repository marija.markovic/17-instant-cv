import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { AuthService } from '../services/auth.service';

import {FormComponent} from '../form/form.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-template',
  templateUrl: './edit-template.component.html',
  styleUrls: ['./edit-template.component.css']
})
@Injectable()
export class EditTemplateComponent implements OnInit {

  public cvForm: FormGroup;
  public isAddressShown = false;

  public isSection = new Map();

  public clickedSections: string[] = [];
  private routeSub: Subscription;
  private templateId: String;
  public pdfPath: string;
  public safePdfPath: SafeResourceUrl;

  public images;

  constructor(private formBuilder: FormBuilder,
              private http: HttpClient,
              private activeRoute: ActivatedRoute,
              private auth_service: AuthService,
              private sanitizer: DomSanitizer ) {

    this.isSection.set('Education', false).set('WorkExperience', false)
                  .set('Volunteering', false).set('Projects', false)
                  .set('Skills', false).set('Publications', false)
                  .set('Achievements', false).set('Talks', false)
                  .set('Course', false).set('Interests', false)
                  .set('Hobby', false);

    this.cvForm = this.formBuilder.group({
      Name: ['', [Validators.required]],
      LastName: ['', [Validators.required]],
      LinkedIn: [''],
      Email: ['', [Validators.required, Validators.email]],
      MobilePhone: ['', [Validators.required]],
      Street: [''],
      City: [''],
      Country: [''],
      Language: [''],
      Color: [''],
      Education: new FormArray([]),
      WorkExperience: new FormArray([]),
      Volunteering: new FormArray([]),
      ProfessionalSkills: new FormArray([]),
      Projects: new FormArray([]),
      Publications: new FormArray([]),
      Achievements: new FormArray([]),
      Talks: new FormArray([]),
      Courses: new FormArray([]),
      Interests: new FormArray([]),
      Hobbies: new FormArray([]),
      Picture: ['']
    });
   }

  ngOnInit() {
      this.pdfPath = 'http://localhost:3001/api/images/cv_storage/main.pdf';
      this.safePdfPath = this.sanitizer.bypassSecurityTrustResourceUrl(this.pdfPath);
      this.routeSub = this.activeRoute.params.subscribe(params => {

      this.templateId = params['templateId'];
      this.auth_service.updateTokenExpiry('auth-token');
    });
  }

  ngOnDestroy() {
    this.pdfPath = 'http://localhost:3001/api/images/cv_storage/main.pdf';
    this.routeSub.unsubscribe();
  }

  addressShow() {
      this.isAddressShown = !this.isAddressShown;
  }

  // Image
  selectImage(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.images = file;
    }
  }

  // submit
  public submitInfo(data) {
    this.auth_service.updateTokenExpiry('auth-token');
    if (!this.cvForm.valid) {
      window.alert('Some fields are not valid!');
      return;
    }

    /* Sending data to server - json */
    const dataToSend = this.createJsonToSend(data);

    console.log('CV bez slike:');
    console.log(dataToSend);

    if (this.images && Number(this.templateId) <= 5){
      const formData = new FormData();
      formData.append('file', this.images);

      /* Sending data to server - image */
      this.http.post<any>(`${environment.serverUrl}/cv/create_pdf/images/${this.templateId}`,
                              formData).subscribe(
            (res) => console.log(res),
            (err) => console.log(err)
          );
    }

    this.http.post<any>(`${environment.serverUrl}/cv/create_pdf/final/${this.templateId}`,
                        dataToSend).subscribe(data => {
                          console.log(data.path);

                          let elem = window.document.createElement('a');
                          elem.href = `${environment.serverUrl}/cv/create_pdf/download/${data.path}`;
                          elem.download = `${dataToSend.Name}${dataToSend.LastName}.zip`;
                          document.body.appendChild(elem);
                          elem.click();
                          document.body.removeChild(elem);

                          console.log('PDF is Ready');
                        },
    err => {
      console.log(err.error);
    });


  }

  public previewInfo(data){
    this.auth_service.updateTokenExpiry('auth-token');
    if(!this.cvForm.valid) {
      window.alert('Some fields are not valid!');
      return;
    }

    const dataToSend = this.createJsonToSend(data);


    if(this.images  && Number(this.templateId) <= 5){
      const formData = new FormData();
      formData.append('file', this.images);

      /* Sending data to server - image */

      this.http.post<any>(`${environment.serverUrl}/cv/create_pdf/images/${this.templateId}`,
                          formData).subscribe(
        (res) => {console.log(res);},
        (err) => console.log(err)
      );
    }

    this.http.post<any>(`${environment.serverUrl}/cv/create_pdf/preview/${this.templateId}`,
                        dataToSend).subscribe(data => {
                          console.log('PDF is Ready');
                          this.pdfPath = environment.serverUrl + '/images/' + data.path;
                          this.safePdfPath = this.sanitizer.bypassSecurityTrustResourceUrl(this.pdfPath);
                          this.reload();
                        },
    err => {
      console.log(err.error);
    });

  }

  private reload(){
    const container = document.getElementById('preview');
    const content = container.innerHTML;
    container.innerHTML = content;

    console.log('Refreshed');
  }
  public createJsonToSend(data: Object) {
      let userID = this.auth_service.getID();
      let returnData = {UserID : userID ? userID : 'guest',
                        Name :      data['Name'],
                        LastName :  data['LastName'],
                        Address :   data['Street'],
                        City :      data['City'],
                        Country :   data['Country'],
                        Phone:      data['MobilePhone'],
                        Email :     data['Email'],
                        LinkedIn :  data['LinkedIn'],
                        Languages : data['Language'],
                        Color:      data['Color'],
                        Picture:    this.images ? this.images.name : ''
                        };

      let clicked;

      // tslint:disable-next-line: forin
      for (clicked in this.clickedSections) {
        let section = this.clickedSections[clicked]
        let sectionSize = data[section].length;

        let sectionJson = {}

        let i;

        for(i = 0; i < sectionSize; i++) {
           sectionJson[`${section}${i}`] = data[section][i].value
        }
        returnData[section] = sectionJson;
      }

      return returnData;
  }


  // getters
  public getTemplateId() {
    return Number(this.templateId);
  }

  public email() {
    return this.cvForm.get('Email');
  }

  public name() {
    return this.cvForm.get('Name');
  }

  public lastName() {
    return this.cvForm.get('LastName');
  }

  // Section methods
  addEducation() {
      const eduGroup = new FormGroup({
        year: new FormControl(''),
        school: new FormControl(''),
        description: new FormControl('')
      });

      this.cvForm.controls['Education'].value.push(eduGroup);

      if(!(this.clickedSections.indexOf('Education') > -1)) {
        this.clickedSections.push('Education');
        this.isSection['Education'] = true;
      }

  }

  addWorkExperience() {
      const expGroup = new FormGroup({
        year: new FormControl(''),
        company: new FormControl(''),
        position: new FormControl(''),
        description: new FormControl('')
      });

      this.cvForm.controls['WorkExperience'].value.push(expGroup);

      if(!(this.clickedSections.indexOf('WorkExperience') > -1)) {
        this.clickedSections.push('WorkExperience');
        this.isSection['WorkExperience'] = true;
      }
  }

  addProjects() {
    const proGroup = new FormGroup({
      year: new FormControl(''),
      name: new FormControl(''),
      type: new FormControl(''),
      description: new FormControl('')
    });


    this.cvForm.controls['Projects'].value.push(proGroup);

    if(!(this.clickedSections.indexOf('Projects') > -1)) {
      this.clickedSections.push('Projects');
      this.isSection['Projects'] = true;
    }

  }

  addVolunteering() {
    const volGroup = new FormGroup({
      year: new FormControl(''),
      company: new FormControl(''),
      description: new FormControl(''),
    });

    this.cvForm.controls['Volunteering'].value.push(volGroup);

    if(!(this.clickedSections.indexOf('Volunteering') > -1)) {
      this.clickedSections.push('Volunteering');
      this.isSection['Volunteering'] = true;
    }
  }

  addProfessionalSkills() {
    const skillGroup = new FormGroup({
      skill: new FormControl(''),
    });

    this.cvForm.controls['ProfessionalSkills'].value.push(skillGroup);
    if(!(this.clickedSections.indexOf('ProfessionalSkills') > -1)) {
      this.clickedSections.push('ProfessionalSkills');
      this.isSection['Skills'] = true;
    }
  }

  addPublications() {
    const pubGroup = new FormGroup({
      year: new FormControl(''),
      name: new FormControl(''),
      description: new FormControl('')
    });

    this.cvForm.controls['Publications'].value.push(pubGroup);

    if(!(this.clickedSections.indexOf('Publications') > -1)) {
      this.clickedSections.push('Publications');
      this.isSection['Publications'] = true;
    }
  }

  addAchievements() {
    const achGroup = new FormGroup({
      year: new FormControl(''),
      name: new FormControl(''),
      description: new FormControl('')
    });

    this.cvForm.controls['Achievements'].value.push(achGroup);

    if(!(this.clickedSections.indexOf('Achievements') > -1)) {
      this.clickedSections.push('Achievements');
      this.isSection['Achievements'] = true;
    }
  }

  addTalks() {
    const talkGroup = new FormGroup({
      year: new FormControl(''),
      description: new FormControl('')
    });

    this.cvForm.controls['Talks'].value.push(talkGroup);

    if(!(this.clickedSections.indexOf('Talks') > -1)) {
      this.clickedSections.push('Talks');
      this.isSection['Talks'] = true;
    }
  }

  addCourses() {
    const courseGroup = new FormGroup({
      year: new FormControl(''),
      name: new FormControl(''),
    });

    this.cvForm.controls['Courses'].value.push(courseGroup);

    if(!(this.clickedSections.indexOf('Courses') > -1)) {
      this.clickedSections.push('Courses');
      this.isSection['Courses'] = true;
    }
  }

  addInterests() {
    const interestGroup = new FormGroup({
      description: new FormControl('')
    });

    this.cvForm.controls['Interests'].value.push(interestGroup);

    if(!(this.clickedSections.indexOf('Interests') > -1)) {
      this.clickedSections.push('Interests');
      this.isSection['Interests'] = true;
    }
  }

  addHobbies() {
    const hobbyGroup = new FormGroup({
      description: new FormControl('')
    });

    this.cvForm.controls['Hobbies'].value.push(hobbyGroup);

    if(!(this.clickedSections.indexOf('Hobbies') > -1)) {
      this.clickedSections.push('Hobbies');
      this.isSection['Hobby'] = true;
    }
  }


}

