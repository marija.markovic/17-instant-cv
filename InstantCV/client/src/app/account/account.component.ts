import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { AuthService } from '../services/auth.service';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  public editName: FormGroup;
  public editEmail: FormGroup;
  public editPass: FormGroup;
  public deleteAcc: FormGroup;

  public hide:boolean = true;

  validPass: boolean;
  validEmail: boolean;
  validName: boolean;
  validUser: boolean;

  successName: boolean;
  successEmail: boolean;
  successPass: boolean;

  constructor(private formBuilder: FormBuilder,
              private _router: Router,
              private http : HttpClient,
              private authService: AuthService){

      this.editName = this.formBuilder.group({
      name: ['']
    });

      this.editEmail = this.formBuilder.group({
      email: ['',  [Validators.email]]
    });

      this.editPass = this.formBuilder.group({
      password: ['', [Validators.minLength(8)]]
    });

    this.deleteAcc = this.formBuilder.group({
    });

      this.validUser = true;
      this.validPass = true;
      this.validEmail = true;
      this.validName = true;

      this.successName = false;
      this.successEmail = false;
      this.successPass = false;
  }

  ngOnInit() {
    this.http.get<any>(environment.serverUrl + '/user/update/info',{'headers': this.authService.setTokenHeader()}).subscribe(data => {
      //console.log(data);
      this.editName.get('name').setValue(data.name);
      this.editEmail.get('email').setValue(data.email);
    },
    err => {
      //console.log(err.error);
      this._router.navigateByUrl("/");
      window.alert('Access denied!');
    });
    this.authService.updateTokenExpiry('auth-token');
  }

  public submitEditName(data){
    //console.log(data);

    if(!this.editName.valid) {
      window.alert('Not valid!');
      return;
    }
    this.http.post<any>(environment.serverUrl + '/user/update/name', data,{'headers': this.authService.setTokenHeader()}).subscribe(data => {

      this.successName = true;
      this.validName = true;
    },
    err => {
      this.successName = false;

      if (err.error === 'Name already exists'){
        this.validName = false;
      }
    });
  }

  public submitEditEmail(data){

    if(!this.editEmail.valid) {
      window.alert('Not valid!');
      return;
    }
    this.http.post<any>(environment.serverUrl + '/user/update/email', data,{'headers': this.authService.setTokenHeader()}).subscribe(data => {
      this.successEmail = true;
      this.validEmail = true;
    },
    err => {
      this.successEmail = false;

      if (err.error === 'Email already exists'){
        this.validEmail = false;
      }
    });
  }
  public submitEditPass(data){

    if(!this.editPass.valid) {
      window.alert('Not valid!');
      return;
    }
    this.http.post<any>(environment.serverUrl + '/user/update/password', data,{'headers': this.authService.setTokenHeader()}).subscribe(data => {
      //console.log(data);
      this.successPass = true;
      this.validPass = true;
      //this.editName.reset();
    },
    err => {
      this.successPass = false;
      //console.log(err.error);

      if(err.error === 'New password is the same as old'){
        this.validPass = false;
      }
    });
  }

  public DeleteAccount(){
    this.http.get<any>(environment.serverUrl + '/user/update/delete',{'headers': this.authService.setTokenHeader()}).subscribe(data => {
      console.log(data.text);
      this.authService.logout();
    },
    err => {
      console.log(err.error);
    });
  }

  public modalOpen(){
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
  }

  public modalClose(){
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
  }

  public editMail(){
    alert('LALALALA');
  }

  public editNameGet(){
    return this.editName.get('name');
  }

  public editEmailGet(){
    return this.editEmail.get('email');
  }

  public editPassGet(){
    return this.editPass.get('password');
  }

}
