const router = require('express').Router();
const fs = require('fs');


router.get('/cv_storage/:fileName', (req, res) =>{
    var data =fs.readFileSync(`./cv_storage/${req.params.fileName}`);
    res.contentType("application/pdf");
    res.send(data);
})

router.get('/templates/:templateFolder/:fileName', (req, res) =>{
    var data = fs.readFileSync(
        `./templates/${req.params.templateFolder}/${req.params.fileName}`);
    
    res.contentType("application/pdf");
    res.send(data);
})

module.exports = router;