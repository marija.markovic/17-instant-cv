const router = require('express').Router();
const controller = require('../../controllers/creationController');
const multer = require('multer');


router.post('/final/:cvID', controller.submitJSON);

router.post('/preview/:cvID', controller.previewJSON);

router.get('/download/:templateId', controller.downloadZip);

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, `templates/template${req.params.cvID}`);
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
});

const upload = multer({ storage: storage });

router.post('/images/:cvID', upload.single('file'), controller.createPicture);


module.exports = router;
