const fs = require('fs');
const transform = require('../functions/transform/transform');

async function JSONtoTEX(jsonData){
    console.log('from JSONtoTEX: ', jsonData);

    let addedLanguages = false;
    let addedLinkedIn = false;
    let cvColor = jsonData.Color === '' ? {r: 111, g: 156, b:45} : transform.hexToRgb(jsonData.Color);

    let intro =
    `\\documentclass[helvetica,english,logo,notitle,totpages,utf8]{europecv2013}
\\usepackage{graphicx}
\\usepackage[a4paper,top=1.2cm,left=1.2cm,right=1.2cm,bottom=2.5cm]{geometry}
\\usepackage[english]{babel}
\\usepackage[T1]{fontenc}
\\usepackage{natbib}
\\usepackage{bibentry}

\\ecvname{${jsonData.Name} ${jsonData.LastName}}
${makeAddress(jsonData)}
\\ecvtelephone[]{${transform.escapeCharacters(jsonData.Phone)}}
\\ecvemail{${transform.escapeCharacters(jsonData.Email)}}
${makeLinkedIn(jsonData.LinkedIn)}

\\begin{document}
\\selectlanguage{english}

\\begin{europecv}
\\ecvpersonalinfo[10pt]

    `;

    let sectionsPart = '';
    const keys = Object.keys(jsonData);

    if(!addedLanguages && jsonData.Languages !== ''){
      sectionsPart += makeLanguages(jsonData.Languages) + '\n';
      addedLanguages = true;
    }


    for(let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++){
        let section = keys[i];

        let sectionString = makeSection(section, jsonData[section]);
        sectionsPart += sectionString + '\n';
    }

    texString = intro + sectionsPart + '\\end{europecv}\n \\end{document}\n';

    await fs.writeFile(`./templates/template8/${jsonData.Name}${jsonData.LastName}.tex`,
     texString, function (err) {
        if(err) return console.log(err);
        console.log("Pravljenje tex fajla.");
    });
}


function makeSection(section, data){
    let retVal = '';
    let sectionStart = `\\ecvsection{${section}}\n`;

    switch(section){
        case 'Education':
            retVal = makeEdu(data);
            break;
        case 'WorkExperience':
            sectionStart = `\\ecvsection{Work experience}\n`
            retVal = makeExp(data);
            break;
        case 'Volunteering':
            retVal = makeVolunteering(data);
            break;
        case 'ProfessionalSkills':
            sectionStart = `\\ecvsection{Professional skills}`
            retVal = makeSkills(data);
            break;
        case 'Projects':
            retVal = makeProjects(data);
           break;
        case 'Publications':
            retVal = makePublications(data);
            break;
        case 'Achievements':
            retVal = makeAchievements(data);
            break;
        case 'Talks':
            retVal = makeTalks(data);
            break;
        case 'Courses':
            retVal = makeCourses(data);
            break;
        case 'Interests':
            retVal = makeInterests(data);
            break;
        case 'Hobbies':
            retVal = makeHobbies(data);
            break;
        }

        return sectionStart + '\n' +retVal;
}

function makeAddress(data){
    let array = [];

    if(data.Address !== ''){
        array.push(` ${data.Address}`);
    }
    if(data.City !== ''){
        array.push(`${data.City}`);
    }
    if(data.Country !== ''){
        array.push(`${data.Country}`);
    }

    return array.length > 0 ? `\\ecvaddress{${transform.escapeCharacters(data.Address)}, ${transform.escapeCharacters(data.City)}, ${transform.escapeCharacters(data.Country)}}` : '';
}

function makeLinkedIn(linkedin) {
    return (linkedin !== '') ? `\\ecvlinkedin{\\href{}{${transform.escapeCharacters(linkedin)}}}` : '';
}

function makeLanguages(languages){

    let section = `%LANGUAGES SECTION\n\\ecvsection{Languages}\n\n`;

    if(languages !== ''){
        langs = languages.split(', ');

        for(let lang in langs){
            language = langs[lang].substr(0, langs[lang].indexOf(' '));
            level = langs[lang].substr(langs[lang].indexOf(' ') + 1);

            section += `\\ecvitem[10pt]{${transform.escapeCharacters(language)}}{${transform.escapeCharacters(level)}}\n`;
        }
    }

    return section + `\\ecvlanguagefooter[10pt]\n`;
}

function makeEdu(education){

    const keys = Object.keys(education);
    let eduList = '';


    for(let key in keys){
        eduList +=
        `\\ecveducation{${transform.escapeCharacters(education[keys[key]].year)}}{${transform.escapeCharacters(education[keys[key]].school)}}{${transform.escapeCharacters(education[keys[key]].description)}}{}{}

        `;
    }
    return eduList;
}

function makeExp(experiences){

    let expList = '';
    let keys = Object.keys(experiences);

    for(let key in keys){
        expList +=
        `\\ecvworkexperience{${transform.escapeCharacters(experiences[keys[key]].year)}}{${transform.escapeCharacters(experiences[keys[key]].position)}}{${transform.escapeCharacters(experiences[keys[key]].company)}}{}{${transform.escapeCharacters(experiences[keys[key]].description)}\\par}

        `;
    }

    return expList;
}

function makeVolunteering(volunteering){
    let volsList = '';
    const keys = Object.keys(volunteering);

    for(let key in keys){
        volsList +=
        `\\ecveducation{${transform.escapeCharacters(volunteering[keys[key]].year)}}{${transform.escapeCharacters(volunteering[keys[key]].company)}}{${transform.escapeCharacters(volunteering[keys[key]].description)}}{}{}

        `;
    }

    return volsList;
}

function makeProjects(projects){

    let projectsList = '';
    const keys = Object.keys(projects);

    for(let key in keys){
        projectsList +=
        `\\ecvworkexperience{${transform.escapeCharacters(projects[keys[key]].year)}}{${transform.escapeCharacters(projects[keys[key]].name)}}{${transform.escapeCharacters(projects[keys[key]].type)}}{}{${transform.escapeCharacters(projects[keys[key]].description)}\\par}

        `
    }

    return projectsList;
}

function makeSkills(skills){

    let skillList = '';
    let keys = Object.keys(skills);

    for(let key in keys){
      skillList +=
      `\\ecvitem[10pt]{}{${transform.escapeCharacters(skills[keys[key]].skill)}}

      `;
    }

    return skillList;
}

function makeAchievements(achievements){

    let achievementsList = '';
    const keys = Object.keys(achievements);

    for(let key in keys){
        achievementsList +=
        `\\ecveducation{${transform.escapeCharacters(achievements[keys[key]].year)}}{${transform.escapeCharacters(achievements[keys[key]].name)}}{${transform.escapeCharacters(achievements[keys[key]].description)}}{}{}

        `;
    }

    return achievementsList;
}

function makeCourses(courses){

    let coursesList = '';
    const keys = Object.keys(courses);

    for(let key in keys){
        coursesList +=
        `\\ecvitem[10pt]{${transform.escapeCharacters(courses[keys[key]].year)}}{${transform.escapeCharacters(courses[keys[key]].name)}}

        `;
    }

    return coursesList;
}

function makeHobbies(hobbies){
    const keys = Object.keys(hobbies);

    return `\\ecvitem[10pt]{}{${transform.escapeCharacters(hobbies[keys[0]].description)}}`;
}

function makeInterests(interests){
    const keys = Object.keys(interests);

    return `\\ecvitem[10pt]{}{${transform.escapeCharacters(interests[keys[0]].description)}}`;
}

function makePublications(publications){

    let publicationsList = '';
    const keys = Object.keys(publications);

    for(let key in keys){
        publicationsList +=
        `\\ecveducation{${transform.escapeCharacters(publications[keys[key]].year)}}{${transform.escapeCharacters(publications[keys[key]].name)}}{${transform.escapeCharacters(publications[keys[key]].description)}}{}{}

        `;
    }

    return publicationsList;
}


function makeTalks(talks){
    let talksList = '';
    const keys = Object.keys(talks);

    for(let key in keys){
        talksList +=
        `\\ecvitem[10pt]{${transform.escapeCharacters(talks[keys[key]].year)}}{${transform.escapeCharacters(talks[keys[key]].description)}}

        `;
    }

    return talksList;
}

module.exports = {JSONtoTEX};
