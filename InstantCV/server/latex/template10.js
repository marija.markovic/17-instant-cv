    const fs = require('fs');
     const dotenv = require('dotenv');
     const transform = require('../functions/transform/transform');


     async function JSONtoTEX(jsonData){
         console.log('from JSONtoTEX: ', jsonData);

         let addedLanguages = false;
         let cvColor = jsonData.Color === '' ? {r: 111, g: 156, b:45} : transform.hexToRgb(jsonData.Color);

     let intro =
         `\\documentclass[11pt,a4paper,sans]{moderncv}
         \\moderncvstyle{banking}

         \\newcommand{\\mycvcolor}[1]{
          \\definecolor{color0}{rgb}{0,0,0}
          \\definecolor{color1}{RGB}{#1}
          \\definecolor{color2}{rgb}{0.45,0.45,0.45}
        }

         \\mycvcolor{${cvColor.r}, ${cvColor.g}, ${cvColor.b}}
         \\usepackage{lipsum}
         \\usepackage[utf8]{inputenc}

         \\usepackage[scale=0.8]{geometry}
         \\usepackage{import}
         \\usepackage{bbding}
         \\usepackage{xcolor}
         \\usepackage{fontawesome}

         \\firstname{${jsonData.Name}} % Your first name
         \\familyname{${jsonData.LastName}} % Your last name
         \\title{CV}
         \\mobile{${transform.escapeCharacters(jsonData.Phone)}}
         ${makeAddress(jsonData)}

         \\begin{document}

         \\makecvtitle

         \\textcolor{gray}{\\em {\\Envelope  ${transform.escapeCharacters(jsonData.Email)}} \\hfill \\faLinkedinSquare  ${transform.escapeCharacters(jsonData.LinkedIn)}}

         `;

         let sectionsPart = '';
         const keys = Object.keys(jsonData);

         if(!addedLanguages && jsonData.Languages !==''){
          sectionsPart += makeLanguages(jsonData.Languages) + '\n';
          addedLanguages = true;
}
         for(let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++){
             let section = keys[i];

             let sectionString = makeSection(section, jsonData[section]);
             sectionsPart += sectionString + '\n';
         }

         texString = intro + sectionsPart + '\\end{document}\n';

         await fs.writeFile(`./templates/template10/${jsonData.Name}${jsonData.LastName}.tex`,
          texString

     , function (err) {
        if(err) return console.log(err);
        console.log("Pravljenje tex fajla.");
    });
}


function makeSection(section, data){
    let retVal = '';
    let sectionStart = `\\section{${section.toLowerCase()}}`;

    switch(section){
        case 'Education':
            retVal = makeEdu(data);
            break;
        case 'WorkExperience':
            sectionStart = `\\section{work experience}`
            retVal = makeExp(data);
            break;
        case 'Volunteering':
            retVal = makeVolunteering(data);
            break;
        case 'ProfessionalSkills':
            sectionStart = `\\section{professional skills}`
            retVal = makeSkills(data);
            break;
        case 'Projects':
            retVal = makeProjects(data);
           break;
        case 'Publications':
            retVal = makePublications(data);
            break;
        case 'Achievements':
            retVal = makeAchievements(data);
            break;
        case 'Talks':
            retVal = makeTalks(data);
            break;
        case 'Courses':
            retVal = makeCourses(data);
            break;
        case 'Interests':
            retVal = makeInterests(data);
            break;
        case 'Hobbies':
            retVal = makeHobbies(data);
            break;
        }

        return sectionStart + '\n' +retVal;
}

function makeAddress(data){
  let array = [];

  if(data.Address !== ''){
      array.push(` ${data.Address}`);
  }
  if(data.City !== ''){
      array.push(`${data.City}`);
  }
  if(data.Country !== ''){
      array.push(`${data.Country}`);
  }

  return array.length > 0 ? `\\address{${transform.escapeCharacters(data.Address)}}{${transform.escapeCharacters(data.City)}, ${transform.escapeCharacters(data.Country)}}` : '';
}


function makeEdu(education){

    let eduList = '';
    const keys = Object.keys(education);

    for(let key in keys){
        eduList += entry(education[keys[key]].year,
                         education[keys[key]].school,
                         education[keys[key]].description,
                         '');
    }

    return eduList;
}

function makeAchievements(achievements){

    let achievementsList = '';
    const keys = Object.keys(achievements);

    for(let key in keys){
        achievementsList += entry(achievements[keys[key]].year,
            achievements[keys[key]].name,
            '',
            achievements[keys[key]].description);
    }

    return achievementsList;
}
function makeCourses(courses){

    let coursesList = '';
    const keys = Object.keys(courses);

    for(let key in keys){
        coursesList += entry(courses[keys[key]].year,
                             courses[keys[key]].name,
                             '',
                             '');
    }

    return coursesList;
}

function makeExp(experiences){

    let expList = '';
    let keys = Object.keys(experiences);

    for(let key in keys){
        expList += entry(experiences[keys[key]].year,
            experiences[keys[key]].company,
            experiences[keys[key]].position,
            experiences[keys[key]].description);
    }

    return expList;
}

function makeHobbies(hobbies){
    const keys = Object.keys(hobbies);

    return transform.escapeCharacters(hobbies[keys[0]].description);
}

function makeInterests(interests){
    const keys = Object.keys(interests);

    return transform.escapeCharacters(interests[keys[0]].description);
}

function makeProjects(projects){

    let projectsList = '';
    const keys = Object.keys(projects);

    for(let key in keys){
        projectsList += entry(projects[keys[key]].year,
                              projects[keys[key]].name,
                              projects[keys[key]].type,
                              projects[keys[key]].description) + `\\\\`;
    }

    return projectsList;
}

function makePublications(publications){

    let publicationsList = '';
    const keys = Object.keys(publications);

    for(let key in keys){
        publicationsList += entry(publications[keys[key]].year,
                                  publications[keys[key]].name,
                                  '',
                                  publications[keys[key]].description);
    }

    return publicationsList;
}

function makeSkills(skills){

    let skillArray = [];
    let keys = Object.keys(skills);

    for(let key in keys){
        skillArray.push(transform.escapeCharacters(skills[keys[key]].skill));
    }

    return skillArray.join(', ');
}

function makeTalks(talks){
    let talksList = '';
    const keys = Object.keys(talks);

    for(let key in keys){
        talksList += entry(talks[keys[key]].year, talks[keys[key]].description, '', '');
    }

    return talksList;
}

function makeVolunteering(volunteering){
    let volsList = '';
    const keys = Object.keys(volunteering);

    for(let key in keys){
        volsList += entry(volunteering[keys[key]].year,
                          volunteering[keys[key]].company,
                          '',
                          volunteering[keys[key]].description);
    }

    return volsList;
}

function makeLanguages(languages){

    let section = '\\section{languages}\n';

    if(languages !== ''){

        section += '\\begin{itemize}\n';
        langs = languages.split(', ');

        for(let lang in langs){
            language = langs[lang].split(' ');
            section += `\\item ${transform.escapeCharacters(language[0])} - ${transform.escapeCharacters(language.slice(1).join(' '))}\\\\ \n`;
        }
        section += '\\end{itemize}\n';
    }

    return section;
}

function entry(param1, param2, param3, param4){
    let param4String = param4 !== '' ? `{${transform.escapeCharacters(param4)}}` : '';

    return `\\cventry{}{}{${transform.escapeCharacters(param2)}}{${transform.escapeCharacters(param1)}\\vspace{-1.0em}}{}${param4String}{${transform.escapeCharacters(param3)}}`;
}

module.exports = {JSONtoTEX};
