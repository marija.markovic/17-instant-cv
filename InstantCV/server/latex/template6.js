const fs = require('fs');
const transform = require('../functions/transform/transform');




async function JSONtoTEX(jsonData){
    console.log('from JSONtoTEX: ', jsonData);
    intro = `
    \\documentclass[a4paper]{cv-style}
    \\sethyphenation[variant=british]{english}{}

    \\begin{document}

    \\header{${jsonData.Name} ${jsonData.LastName}}{}\n

    `

    const asideBegin = `\\begin{aside}\n`;
    const asideEnd = `\\end{aside}\n`;

    const mail = jsonData.Email;
    const phone = jsonData.Phone;
    const address = jsonData.Address;
    const city = jsonData.City;
    const country = jsonData.Country;
    const languages = jsonData.Languages;
    const linkedIn = jsonData.LinkedIn;
    const skills = jsonData.ProfessionalSkills;

    let contactString = makeAddress(address, city, country);
    let phoneString = makePhone(phone);
    let mailString = makeMail(mail);
    let linkedInString = makeLinkedIn(linkedIn);
    let skillsString = skills == undefined ? '' : makeSkills(skills);

    let languagesString = makeLanguages(languages);

    const asideString =
    asideBegin +
    contactString +
    phoneString +
    mailString +
    linkedInString +
    languagesString +
    skillsString +
    asideEnd;


    let sectionsPart = '';
    const keys = Object.keys(jsonData);
    for(let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++){
        let section = keys[i];
        if(section === 'ProfessionalSkills')
            continue;
        let sectionString = makeSection(section, jsonData[section]);
        sectionsPart += sectionString + '\n';
    }

    let texString = intro + asideString + '\n' + sectionsPart + '\n' + '\\end{document}';

    await fs.writeFile(`./templates/template6/${jsonData.Name}${jsonData.LastName}.tex`,
     texString, function (err) {
        if(err) return console.log(err);
        console.log("Pravljenje tex fajla.");
    });
}


function makeSection(section, data){
    let retVal = '';
    let sectionStart = `\\section{${section}}`;

    switch(section){
        case 'Education':
            retVal = makeEdu(data);
            break;
        case 'WorkExperience':
            sectionStart = '\\section{Experience}\n';
            retVal = makeExp(data);
            break;
        case 'Volunteering':
            retVal = makeVolunteering(data);
            break;
        case 'Projects':
            retVal = makeProjects(data);
           break;
        case 'Publications':
            retVal = makePublications(data);
            break;
        case 'Achievements':
            retVal = makeAchievements(data);
            break;
        case 'Talks':
            retVal = makeTalks(data);
            break;
        case 'Courses':
            retVal = makeCourses(data);
            break;
        case 'Interests':
            retVal = makeInterests(data);
            break;
        case 'Hobbies':
            retVal = makeHobbies(data);
            break;
        }

        return sectionStart + '\n' +retVal;
}

function makeAddress(address, city, country){
    let contactString = '';
    if(address != '' || city != '' || country != ''){
        contactString =
        `\\section{Contact}
        ${transform.escapeCharacters(address)}
        ${transform.escapeCharacters(city)}
        ${transform.escapeCharacters(country)}
        ~
        `;
    }

    return contactString;
}

function makePhone(phone){
    let phoneString = '';

    if(phone != ''){
        phoneString =
        `Mobile no:
        ${transform.escapeCharacters(phone)}
        ~
        `;
    }

    return phoneString;
}

function makeMail(email){
    let mailString = '';

    if(email != ''){
        mailString =
        `Email:
        ${transform.escapeCharacters(email)}
        ~
        `;
    }

    return mailString;
}

function makeLinkedIn(linkedIn){
    let linString = '';

    if(linkedIn != ''){
        linString =
        `LinkedIn:
        ${transform.escapeCharacters(linkedIn)}
        ~
        `;
    }

    return linString;
}

function makeLanguages(languages){
    let languagesString = '';

    if(languages != ''){
        let langs = languages.split(", ");

        let i;
        languagesString =
        `Languages:
        `;
        for(i=0; i < langs.length; i++){
            languagesString +=
            `${transform.escapeCharacters(langs[i])}
            `;
        }

        languagesString += `~\n`;
    }
    return languagesString;
}

function makeEdu(education){
    let eduList = '';
    const keys = Object.keys(education);

    for(let key in keys){
        eduList += `\\entry{${transform.escapeCharacters(education[keys[key]].year)}}
        {${transform.escapeCharacters(education[keys[key]].school)}}
        {${transform.escapeCharacters( education[keys[key]].description)}}
        {\\vspace{-0.3cm}}
        `
    }

    return entryList(eduList);
}


function makeExp(experience){
    let expList = '';
    const keys = Object.keys(experience);

    for(let key in keys){
        expList += entry(experience[keys[key]].year,
                         experience[keys[key]].company,
                         experience[keys[key]].position,
                         experience[keys[key]].description);
    }

    return entryList(expList);
}


function makeProjects(projects){
    let projectsList = '';
    const keys = Object.keys(projects);

    for(let key in keys){
        projectsList += entry(projects[keys[key]].year,
                              projects[keys[key]].name,
                              projects[keys[key]].type,
                              projects[keys[key]].description);
    }

    return entryList(projectsList);
}

function makeVolunteering(volunteering){
    let volsList = '';
    const keys = Object.keys(volunteering);

    for(let key in keys){
        volsList += entry(volunteering[keys[key]].year,
                          volunteering[keys[key]].company,
                          '',
                          volunteering[keys[key]].description);
    }

    return entryList(volsList);
}

function makeAchievements(achievements){
    let achievementsList = '';
    const keys = Object.keys(achievements);

    for(let key in keys){
        achievementsList += entry(achievements[keys[key]].year,
                                  achievements[keys[key]].name,
                                 '',
                                achievements[keys[key]].description);
    }

    return entryList(achievementsList);
}


function makeSkills(skills){
    let skillsList = 'Professional Skills: \n';
    const keys = Object.keys(skills);

    for(let key in keys){
        skillsList +=
        `${transform.escapeCharacters(skills[keys[key]].skill)}
        `;
    }

    return skillsList  + '~\n';
}


function makePublications(publications){
    let publicationsList = '';
    const keys = Object.keys(publications);

    for(let key in keys){
        publicationsList += entry(publications[keys[key]].year,
            publications[keys[key]].name,
             publications[keys[key]].description, '');
    }

    return entryList(publicationsList);
}

function makeTalks(talks){
    let talksList = '';
    const keys = Object.keys(talks);

    for(let key in keys){
        talksList += entry(talks[keys[key]].year, talks[keys[key]].description, '', '');
    }

    return entryList(talksList);
}

function makeCourses(courses){
    let coursesList = '';
    const keys = Object.keys(courses);

    for(let key in keys){
        coursesList += entry(courses[keys[key]].year, courses[keys[key]].name, '', '');
    }

    return entryList(coursesList);
}

function makeInterests(interests){
    let interestsList = '';
    const keys = Object.keys(interests);

    for(let key in keys){
        interestsList += entry('', interests[keys[key]].description, '', '');
    }

    return entryList(interestsList);
}

function makeHobbies(hobbies){
    let hobbiesList = '';
    const keys = Object.keys(hobbies);

    for(let key in keys){
        hobbiesList += entry('', hobbies[keys[key]].description, '', '');
    }

    return entryList(hobbiesList);
}


function entry(param1, param2, param3, param4){

    return `\\entry{${transform.escapeCharacters(param1)}}
            {${transform.escapeCharacters(param2)}}
            {${transform.escapeCharacters(param3)}}
            {${transform.escapeCharacters(param4)}}

        `
}

function entryList(list){
    return `   \\begin{entrylist}
                ${list}
        \\end{entrylist}`;
}

module.exports = {JSONtoTEX};
