const util = require('util');
const multer = require('multer');
const AdmZip = require('adm-zip');

const exec = util.promisify(require('child_process').exec);
const Curriculum = require('../model/Curriculum');

const template1  = require("../latex/template1");
const template2  = require("../latex/template2");
const template3  = require("../latex/template3");
const template4  = require("../latex/template4");
const template5  = require("../latex/template5");
const template6  = require("../latex/template6");
const template7  = require("../latex/template7");
const template8  = require("../latex/template8");
const template9  = require("../latex/template9");
const template10 = require("../latex/template10");

const compiler = {
     '1': 'xelatex',
     '2': 'pdflatex',
     '3': 'xelatex',
     '4': 'pdflatex',
     '5': 'pdflatex',
     '6': 'xelatex',
     '7': 'xelatex',
     '8': 'pdflatex',
     '9': 'xelatex',
    '10': 'pdflatex'
};


module.exports.submitJSON = async(req, res) => {
    const templateId = req.params.cvID;


    let jsonToTexFunction = switchTemplate(templateId);

    await jsonToTexFunction(req.body);

    await createPdf(compiler[templateId],
        `./templates/template${templateId}`,
        `${req.body.Name}${req.body.LastName}`);


    const newName = `${req.body.Name}${req.body.LastName}_${Date.now()}.pdf`;

    if (req.body.UserID !== 'guest'){

        cv = new Curriculum({
          userID: req.body.UserID,
          path: `./cv_storage/${newName}`
          });

        await cv.save();

        await storePDF(`./templates/template${templateId}`,
                `${req.body.Name}${req.body.LastName}`,
                newName);
    }

    res.json({path: `template${templateId}`}).status(200);
};

module.exports.downloadZip = async(req, res) => {
  const templateId = req.params.templateId;

  const zip = new AdmZip();

    zip.addLocalFolder(`./templates/${templateId}`);

    // Define zip file name
    const downloadName = `${Date.now()}.zip`;

    const data = zip.toBuffer();

    //code to download zip file
    res.set('Content-Type','application/octet-stream');
    res.set('Content-Disposition',`attachment; filename=${downloadName}`);
    res.set('Content-Length',data.length);
    res.send(data);

    await cleanFolder(`./templates/${templateId}`);
}

module.exports.previewJSON = async(req, res) => {
    const templateId = req.params.cvID;

    let jsonToTexFunction = switchTemplate(templateId);
    await jsonToTexFunction(req.body);
    await createPdf(compiler[templateId],
        `./templates/template${templateId}`,
        `${req.body.Name}${req.body.LastName}`);

    res.json({ path: `/templates/template${templateId}/${req.body.Name}${req.body.LastName}.pdf` });
};


function switchTemplate(templateId) {

    switch (templateId) {
        case '1':
            retval = template1.JSONtoTEX;
            break;
        case '2':
            retval = template2.JSONtoTEX;
            break;
        case '3':
            retval = template3.JSONtoTEX;
            break;
        case '4':
            retval = template4.JSONtoTEX;
            break;
        case '5':
            retval = template5.JSONtoTEX;
            break;
        case '6':
            retval = template6.JSONtoTEX;
            break;
        case '7':
            retval = template7.JSONtoTEX;
            break;
        case '8':
            retval = template8.JSONtoTEX;
            break;
        case '9':
            retval = template9.JSONtoTEX;
            break;
        case '10':
            retval = template10.JSONtoTEX;
            break;
    }
    return retval;
}

module.exports.createPicture = (req, res, next) => {

    const file = req.file;
    console.log(file.filename);

    if (!file) {
        const error = new Error('Too bad')
        error.httpStatusCode = 400
        return next(error)
    }
    res.send(file);
}

async function cleanFolder(path){
  console.log("Cleaning folder");

  try{
    const { stdout, stderr } = await exec(`cd ${path} &&
                                       rm *.tex *.aux *.log *.out *.pdf`);
  }
  catch(err){
    console.log('Nesto ne moze da se obrise, verovatno ne postoji file sa datom ekstenzijom');
  }
}

async function createPdf(command, path, fileName) {
    console.log("Pravljenje pdf-a");

    const { stdout, stderr } = await exec(`cd ${path} &&
                                        ${command} '${fileName}.tex' &&
                                        ${command} '${fileName}.tex'`);
    if (stdout) {
        console.log('PDF is ready');
    }
    if (stderr)
        console.log('stderr:', stderr);
}

async function storePDF(path, fileName, newName) {
    console.log('Cuvanje pdf-a za potrebe baze');

    const { stdout, stderr } = await exec(`cp '${path}/${fileName}.pdf' ./cv_storage/ &&
                              mv './cv_storage/${fileName}.pdf' './cv_storage/${newName}'`);

    if (stderr) {
        console.error('stderr', stderr);
        return;
    } else {
        console.log('PDF is stored');
    }
}
