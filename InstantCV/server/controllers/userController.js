const User = require('../model/User');
const Curriculum = require('../model/Curriculum');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const verify = require('../functions/user/verifyToken');
const fs = require('fs');
const stream = require('stream');
const base64ToImage = require('base64-to-image');


const router = require('express').Router();
const nodemailer = require('nodemailer');
const mailer = require('../functions/user/mailer');
const dotenv = require('dotenv');
const randomstring = require('randomstring');


//REGISTER USER
//reqJSON: email,name,password
//resJSON: _id,name
module.exports.registerFunc = async(req, res) => {
    //checking if user is alreay in DB
    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist)
        return res.status(400).send('Email already exists');
    //name must be unique
    const nameExist = await User.findOne({ name: req.body.name });
    if (nameExist)
        return res.status(400).send('Name already exists');

    //hash password
    const salt = await bcrypt.genSalt(10); //complexity hash function
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const secretToken = randomstring.generate(7);
    console.log("Registration secret token: " + secretToken);
    //create new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
        secretToken: secretToken
    });
    try {
        const savedUser = await user.save();
        const html =
            `Hello,
          <br/>
          https://localhost:4200/validate-account
          Please verify your account by entering the following security code: <br/>
          <b> ${secretToken} </b> <br/>
          on this address: https://localhost:4200/validate-account <br/>
          Have a nice day!`;

        const info = mailer(req.body.email, 'Please verify your email', html);


        console.log('Try2');

        res.json({ 'user': req.body.email }).status(200);
    } catch (err) {
        res.status(400).send(err);

    }


};

module.exports.validationFunc = async(req, res) => {

    const user = await User.findOne({ email: req.body.email });
    if (!user) {
        return res.status(400).send('User not found');
    }

    if (user.secretToken !== req.body.code) {
        console.log(user);
        return res.status(400).send("Code is not correct");
    }
    try {
        await User.updateOne({ email: req.body.email }, { validated: true, secretToken: '' });
    } catch {
        console.log("Error");
    }

    const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);

    res.header('auth-token', token)
        .json({ 'userID': user._id, userToken: token }).status(200);
};

//LOGIN USER
//reqJSON: email,password
//res: 'Logged in!' + in header is JWT as 'auth-token'
module.exports.loginFunc = async(req, res) => {
    //checking if user exists
    console.log(req.body);
    //console.log(res.header());
    const userMail = await User.findOne({ email: req.body.email });
    const userName = await User.findOne({ name: req.body.email });
    if (!userName && !userMail) {
        return res.status(400).send("User don't exists");
    }

    //password check
    user = userMail ? userMail : userName;

    if (!user.validated) {
        return res.status(400).send("User is not validated");
    }

    const validPass = await bcrypt.compare(req.body.password, user.password);

    if (!validPass) {
        return res.status(400).send("Invalid password");
    }

    //Create and assign token
    if (user && validPass) {
        const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
        res.header('auth-token', token).json({ userId: user._id, userToken: token }).status(200);
    }
};

//api/user/update
//**all API's are verified before with middleware function from verifyToken.js
//required header parameter auth-token (received from login request)

//GET USER INFO before update, based on JWT token
//body no parameter
//resJSON: _id,name,email
module.exports.updateInfo = async(req, res) => {
    const user = await User.findOne({ _id: req.user._id });
    if (!user)
        return res.status(400).send('User is not found');
    res.send({ _id: user._id, name: user.name, email: user.email });
};

//CHANGE EMAIL
//reqJSON: email //(new email)
//'You changed email successfully'
module.exports.updateEmail = async(req, res) => {
    const user = await User.findOne({ _id: req.user._id });
    if (!user)
        return res.status(400).send('User is not found');
    //can't use existing email
    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist && user.email != emailExist.email)
        return res.status(400).send('Email already exists');
    try {
        const upd = await User.updateOne({ _id: user._id }, { email: req.body.email })
            //res.status(200).send('You changed email successfully');
        res.json({ text: "You have changed email successfully" }).status(200);
    } catch (err) {
        res.status(400).send(err);
    }
};

//CHANGE NAME
//reqJSON: name //(new name)
//'You changed name successfully'
module.exports.updateName = async(req, res) => {
    console.log(req.user);
    const user = await User.findOne({ _id: req.user._id });
    if (!user)
        return res.status(400).send('User is not found');
    const nameExist = await User.findOne({ name: req.body.name });
    if (nameExist && user.name != nameExist.name)
        return res.status(400).send('Name already exists');
    try {
        const upd = await User.updateOne({ _id: user._id }, { name: req.body.name })
            //res.status(200).send('You changed name successfully');
        res.json({ text: "You have changed name successfully" }).status(200);
    } catch (err) {
        res.status(400).send(err);
    }
};

//CHANGE PASSWORD
//reqJSON: password //(new password)
//'You changed password successfully'
module.exports.updatePass = async(req, res) => {
    const user = await User.findOne({ _id: req.user._id });
    if (!user)
        return res.status(400).send('User is not found');
    const samePass = await bcrypt.compare(req.body.password, user.password);
    if (samePass)
        return res.status(400).send('New password is the same as old');
    else {
        const salt = await bcrypt.genSalt(10); //complexity hash function
        const hashedPassword = await bcrypt.hash(req.body.password, salt);
        try {
            const upd = await User.updateOne({ _id: user._id }, { password: hashedPassword })
            res.json({ text: "You have changed password successfully" }).status(200);
        } catch (err) {
            res.status(400).send(err);
        }
    }
};

//DELETE ACCOUNT with jwt token
module.exports.deleteAcc = async(req, res) => {
    const user = await User.findOne({ _id: req.user._id });
    if (!user)
        return res.status(400).send('User is not found');
    try {
        await User.deleteOne({ _id: req.user._id });
        res.json({ text: "You have deleted account" }).status(200);
    } catch (err) {
        res.status(400).send(err);
    }
};


module.exports.userInfo = async(req, res) => {
    const user = await User.findOne({ _id: req.params.userID });
    const cv_s = await Curriculum.find({ userID: req.params.userID });
    //const cv_s = await Curriculum.find({userID: 'guest'});
    if (!user) {
        res.sendStatus(400);
        return;
    }

    res.json({
        name: user.name,
        email: user.email,
        date: user.date,
        pic: user.picPath,
        _cv: cv_s
    }).status(200);
}



module.exports.profilePic = async(req, res) => {

    const path = req.params.imageName;
    const r = fs.createReadStream(`./profile_images/${path}`);
    const ps = new stream.PassThrough();
    stream.pipeline(
        r,
        ps,
        (err) => {
            if (err) {
                console.log(err)
                return res.sendStatus(400);
            }
        })
    ps.pipe(res)
}

module.exports.updateProfilePic = async(req, res) => {

    let name = 'instantCV' + Date.now();

    let path = 'profile_images/';
    const optionalObj = { fileName: name, type: 'png' };

    const { imageType, fileName } = base64ToImage(req.body.image, path, optionalObj);

    const user = await User.updateOne({ _id: req.params.userId }, { $set: { picPath: fileName } });

    res.status(200);
}

// module.exports.sendUserMail =